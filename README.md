# Prueba IRoute Solutions

La presente aplicación está desarrollada en PHP haciendo de algunas librerías de composer.

# Link del video

[https://youtu.be/CYnoY5vWUbo](https://youtu.be/CYnoY5vWUbo)

## Tecnologías utilizadas

- PHP versión 7.4.16
- Illuminate/database 
- jdzm/jdz-paginator
- Bootstrap

## Despliegue

El front controller de la aplicación se encuentra en public_html (nombre que particularmente utilizan los hosting) en el archivo index.php, el cuál recibirá todas las solicitudes del cliente.

Para para desplegar la aplicación, por ejemplo en un VPS (Virtual Private server) o en cualquier servidor linux, primero se debe ejecutar el comando.

```sh
composer install
```

Posterior a eso puede que sea necesario actualizar la información del cargador automático de clases, para ello es necesario correr el comando:

```sh
composer dump-autoload
```

## Licencia

MIT

**Free Software, Hell Yeah!**