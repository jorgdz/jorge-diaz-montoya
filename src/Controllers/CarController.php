<?php 
namespace Src\Controllers;

use Src\Models\Car;
use Jdzm\JDZPaginator\Pagination\Paginator;

class CarController extends Controller
{
	public function index () {
		if (isset($_SESSION['auth'])) {
			$page = intval((isset($_GET['page']) ? $_GET['page'] : 0));
			$size = intval((isset($_GET['size']) ? $_GET['size'] : 5));
			$offset = $page * $size;
			
			$cars = Car::with('user')
				->orderBy('id', 'desc')->offset($offset)->limit($size)->get();
	
			$count = Car::count();
	
			$paginator = new Paginator($page, $size, $count, $cars);
	
			$this->view('cars/index', ['cars' => $paginator]);
		} else {
			$this->redirect('?view=login&action=index');
		}
	}

	public function create () {
		if (isset($_SESSION['auth'])) {
			$this->view('cars/create');
		} else {
			$this->redirect('?view=login&action=index');
		}
	}

	public function save () {
		if (isset($_SESSION['auth'])) {
			$car = new Car();
			$car->code = $_POST['code'];
			$car->description = $_POST['description'];
			$car->price = $_POST['price'];
			$car->user_id = $_SESSION['auth']['id'];
			$car->save();
	
			$this->redirect('?view=car&action=index', ['message' => 'Carro creado.']);
		} else {
			$this->redirect('?view=login&action=index');
		}
	}

	public function edit () {
		if (isset($_SESSION['auth'])) {
			if (isset($_GET['id'])) {
				$id = $_GET['id'];
				$car = Car::findOrFail($id);
				$this->view('cars/edit', ['car' => $car]);
			}
		} else {
			$this->redirect('?view=login&action=index');
		}
	}

	public function update () {
		if (isset($_SESSION['auth'])) {
			if (isset($_GET['id'])) {
				$id = $_GET['id'];
				$car = Car::findOrFail($id);
				$car->code = $_POST['code'];
				$car->description = $_POST['description'];
				$car->price = $_POST['price'];
				$car->user_id = $_SESSION['auth']['id'];
				$car->save();
				$this->redirect('?view=car&action=index', ['message' => 'Carro modificado.']);
			}
		} else {
			$this->redirect('?view=login&action=index');
		}
	}

	public function destroy () {
		if (isset($_SESSION['auth'])) { 
			if (isset($_GET['id'])) { 
				$id = $_GET['id'];
				$car = Car::findOrFail($id);
				$car->delete();
				$this->redirect('?view=car&action=index', ['message' => 'Carro eliminado!']);
			}
		} else {
			$this->redirect('?view=login&action=index');
		}
	}
}
