<?php 
namespace Src\Controllers;

use Src\Models\User;

class LoginController extends Controller
{
	public function index () {
		$this->view('authentication/index');
	}

    public function access () {
        $user = User::where('email', $_POST['email'])->first();
        if (isset($user)) {
            if (password_verify($_POST['password'], $user->password)) {
                $_SESSION['auth'] = $user;
                $this->redirect('?view=car&action=index');
            } else {
                $this->redirect('?view=login&action=index', ['message' => 'Las credenciales son incorrectas.']);
            }
        } else {
            $this->redirect('?view=login&action=index', ['message' => 'Por favor ingrese las credenciales correctas.']);
        }
    }

    public function logout() {
        unset($_SESSION['auth']);

        $this->redirect('?view=login&action=index');
    }
}
