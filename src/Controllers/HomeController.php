<?php 
namespace Src\Controllers;

class HomeController extends Controller
{
	public function index ()
	{
		if (isset($_SESSION['auth'])) {
			$this->view('home');
		} else {
			$this->redirect('?view=login&action=index');
		}
	}
}
