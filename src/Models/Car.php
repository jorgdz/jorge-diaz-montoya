<?php 
namespace Src\Models;

use Src\Models\User;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
	protected $table = 'cars';

	protected $fillable = ['code', 'description', 'price', 'user_id'];

	public $timestamps = false;

    public function user() {
    	return $this->belongsTo(User::class, 'user_id');
    }
}
