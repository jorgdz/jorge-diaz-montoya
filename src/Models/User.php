<?php 
namespace Src\Models;

use Illuminate\Database\Eloquent\Model;
use Src\Models\Car;

class User extends Model
{
	protected $table = 'users';

	protected $fillable = ['username', 'email', 'password', 'name', 'lastname'];

	public $timestamps = false;

	public function cars () {
        return $this->hasMany(Car::class, 'user_id');
    }
}
