<?php 

use Illuminate\Database\Capsule\Manager;

$manager = new Manager;

$manager->addConnection([
	'driver' => 'mysql',
	'host'	=>  'localhost',
	'username' => 'root',
	'password' => '',
	'database' => 'prueba_jd',
	'port'	=> '3306',
	'charset'	=> 'utf8',
	'prefix'	=> ''
]);

$manager->bootEloquent();