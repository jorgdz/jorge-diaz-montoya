<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Carros</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
	
	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="#">IRoute</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="?view=home&action=index">Inicio <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="?view=car&action=index">Carros</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<?=$_SESSION['auth']['name']?> <?=$_SESSION['auth']['lastname']?>
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="?view=login&action=logout">Cerrar sesión</a>
					</li>
				</ul>
			</div>
		</nav>
		<hr>

		<?php if (isset($_SESSION['message'])) {?>
			<div class="alert alert-success" role="alert">
				<?=$_SESSION['message'];?>
			</div>	
			<?php unset($_SESSION['message']);?>
		<?php }?>


		<a href="?view=car&action=create" class="btn btn-primary">Agregar nuevo</a>
		<hr>
		<table class="table">
			<thead>
				<th>#</th>
				<th>Código</th>
				<th>Descripción</th>
				<th>Precio</th>
				<th>Usuario</th>
				<th>Editar</th>
				<th>Borrar</th>
			</thead>
			<tbody>
				<?php foreach ($cars->data as $car): ?>
					<tr>
						<td><?=$car->id?></td>
						<td><?=$car->code?></td>
						<td><?=$car->description?></td>
						<td><?=$car->price?></td>
						<td><?=$car->user->name?> <?=$car->user->lastname?></td>
						<td>
							<a href="?view=car&action=edit&id=<?=$car->id?>" class="btn btn-warning">Editar</a>
						</td>
						<td>
							<a href="?view=car&action=destroy&id=<?=$car->id?>" onclick="event.preventDefault(); (confirm('¿ ESTAS SEGURO QUE DESEAS ELIMINAR ESTE CARRO ?')) ? document.getElementById('delete-form-<?=$car->id?>').submit() : false;" class="btn btn-danger">Borrar</a>
							<form id="delete-form-<?=$car->id?>" action="?view=car&action=destroy&id=<?=$car->id?>" method="POST" style="display: none;"></form>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>

		<nav aria-label="..." style="float:right;">
		  <ul class="pagination">
		    <?php if ($cars->current_page == $cars->prev): ?>
		    	<li class="page-item disabled">
			      <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Anterior</a>
			    </li>
			<?php else:?>	
				<li class="page-item">
			      <a class="page-link" href="?page=<?=$cars->prev?>">Anterior</a>
			    </li>
		    <?php endif ?>
		    

			<?php foreach ($cars->numbersPage as $number): ?>
				<li class="page-item <?=($cars->current_page == ($number - 1)) ? 'active' : ''?>" aria-current="page">
			      <a class="page-link" href="?page=<?=$number-1?>"><?=$number?></a>
			    </li>				
			<?php endforeach ?>

			
		    <?php if ($cars->current_page == $cars->next): ?>
		    	<li class="page-item disabled">
			      <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Siguiente</a>
			    </li>
			<?php else:?>	
				<li class="page-item">
			      <a class="page-link" href="?page=<?=$cars->next?>">Siguiente</a>
			    </li>
		    <?php endif ?>
		  </ul>
		</nav>
	</div>
	
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>