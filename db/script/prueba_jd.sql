create table users(
    id integer not null primary key AUTO_INCREMENT,
    username varchar(255) not null,
    email varchar(255) not null,
   	password varchar(255) not null,
    name varchar(255) not null,
    lastname varchar (255) not null
);

create table cars (
	id integer not null primary key AUTO_INCREMENT,
    code varchar(255) not null, 
    description text, 
    price float not null,
    user_id integer not null,
    constraint fk_user_cars foreign key(user_id) REFERENCES users(id) on delete CASCADE on UPDATE CASCADE
);

insert into users(username, email, password, name, lastname) values('fperez', 'fperez@mail.com', '$2a$10$HlFUDBXs9EkVq8yXiQ5nYeHr.Nc0Ej4ATzdXx9n7kVAmqY5TyxK2q', 'Fernando', 'Pérez');

insert into cars(code, description, price, user_id) values('DF12', 'Producto comercial 1', 20, 1);
insert into cars(code, description, price, user_id) values('DF13', 'Producto comercial 2', 2.50, 1);
insert into cars(code, description, price, user_id) values('ER45', 'Producto comercial 3', 14.99, 1);
insert into cars(code, description, price, user_id) values('OT62', 'Producto comercial 4', 2.99, 1);