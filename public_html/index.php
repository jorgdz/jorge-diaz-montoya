<?php
require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../config/database.php';
session_start();

$error = 'El recurso solicitado no existe';

if (!isset($_GET['view']) && !isset($_GET['action'])) {
    $name_controller = 'Src\\Controllers\\LoginController';
    $action = 'index';

    $controller = new $name_controller();
    $controller->$action();
} else {
    if (isset($_GET['view'])) {
        $name_controller = 'Src\\Controllers\\'.ucfirst($_GET['view']).'Controller';
    } else {
        echo $error;
        exit();
    }
    
    if (class_exists($name_controller)) {
        $controller = new $name_controller();
    
        if (isset($_GET['action']) && method_exists($controller, $_GET['action'])) {
            $action = $_GET['action'];
            $controller->$action();
        } else {
            echo $error;
        }
    } else {
        echo $error;
    }
}
